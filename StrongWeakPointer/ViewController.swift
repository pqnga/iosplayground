//
//  ViewController.swift
//  StrongWeakPointer
//
//  Created by Nga Pham on 12/16/16.
//  Copyright © 2016 Misfit. All rights reserved.
//

import UIKit

class BLERequest: NSObject {
    
    var completionBlock: (() -> ())?
    
    func run() {
        // Assume that it will finish after 1 second
        // Then we will call "completionBlock" which is defined before
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.completionBlock?()
        }
    }
}

class DeviceViewController: UIViewController {
    
    var currentRequest: BLERequest? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        runRandomRequest_memoryLeak()
        //runRandomRequest_withoutMemoryLeak()
    }
    
    func runRandomRequest_memoryLeak() {
        NSLog("---- runRandomRequest_memoryLeak")
        self.currentRequest = BLERequest()
        
        // Close (dismiss) the view controller when the request finishes
        self.currentRequest?.completionBlock = {
            self.dismiss(animated: true, completion: nil)
        }
        
        // Now, run it
        self.currentRequest?.run()
    }
    
    func runRandomRequest_withoutMemoryLeak() {
        NSLog("---- runRandomRequest_withoutMemoryLeak")
        self.currentRequest = BLERequest()
        
        // Close (dismiss) the view controller when the request finishes
        self.currentRequest?.completionBlock = {[weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        
        // Now, run it
        self.currentRequest?.run()
    }
    
    deinit {
        NSLog("---- object will be deallocated")
    }
}

class ViewController: UIViewController {

    private let runButton: UIButton = {
        let button = UIButton(frame: UIScreen.main.bounds)
        button.setTitle("Run", for: .normal)
        button.backgroundColor = UIColor.darkGray
        return button
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(runButton)
        runButton.addTarget(self, action: #selector(tappedRunButton), for: .touchUpInside)
    }
    
    func tappedRunButton() {
        let vc = DeviceViewController()
        self.present(vc, animated: true, completion: nil)
    }
}

